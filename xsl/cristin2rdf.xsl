<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl [
    <!ENTITY doi "http://dx.doi.org/">
    <!ENTITY bibo "http://purl.org/ontology/bibo/">
    <!ENTITY dct "http://purl.org/dc/terms/">
    <!ENTITY ecrm "http://erlangen-crm.org/current/">
    <!ENTITY mmdr "http://purl.org/momayo/mmdr/"> 
    <!ENTITY mmo "http://purl.org/momayo/mmo/">
    <!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#">
    <!ENTITY upw "http://unpaywall.ub.uib.no/ontology/">
    <!ENTITY vivo "http://vivoweb.org/ontology/core#">
    <!ENTITY upw "http://example.org/ontology/">
    <!ENTITY base "http://unpaywall.ub.uib.no/id/">
    <!ENTITY rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#">
    <!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#">  
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    
    exclude-result-prefixes="xs math"
    version="3.0" expand-text="1">
    <xsl:include href="lib/functions/nq.xsl"/>
    <xsl:strip-space elements="*"/>
    <xsl:include href="lib/functions/uuid.xsl"/>
    <xsl:output indent="no" method="text"/>
    <xsl:variable name="dx-uri" select="'http://dx.doi.org/'" as="xs:string"/>
    <xsl:variable name="graph" select="xs:anyURI('http://unpaywall.ub.uib.no/cristin-data')" as="xs:anyURI"/>
    <xsl:variable name="base-uri" select="'http://unpaywall.ub.uib.no/id/'" as="xs:string"/>
    <xsl:variable name="rdfs-label" select="xs:anyURI('&rdfs;label')" as="xs:anyURI"/>
    <xsl:variable name="rdf-type" select="xs:anyURI('&rdf;type')" as="xs:anyURI"/>
    
    <xsl:template match="row">
        <xsl:variable name="VARARB" select="VARBEIDLOPENR" as="xs:string"/>
        <xsl:variable name="DOI" select="lower-case(DOI)" as="xs:string?"/>
        <xsl:variable name="ISSN" select="upper-case(ISSN)" as="xs:string?"/>
        <xsl:variable name="document-uri" select="xs:anyURI(if (string($DOI)) 
            then concat($dx-uri,encode-for-uri($DOI)) 
            else concat($base-uri,flub:uuid-from-string($VARARB)))" as="xs:anyURI"/>
        <xsl:variable name="journal-seed" select="($ISSN[string(.)],upper-case(KANALNAVN))[1]" as="xs:string"/>
        <xsl:variable name="journal-uri" select="xs:anyURI(concat($base-uri,flub:uuid-from-string($journal-seed)))" 
            as="xs:anyURI"/>
        
        <!-- begin document -->
        <xsl:value-of select="flub:nqobject($document-uri,
            xs:anyURI('&rdf;type'),
            xs:anyURI('&bibo;Document')
            ,$graph)"/>
        <xsl:value-of select="flub:nqobject($document-uri,
            xs:anyURI('&dct;isPartOf'),
            $journal-uri,
            $graph)"/>
        <xsl:value-of select="flub:nqliteral($document-uri,
            xs:anyURI('&bibo;doi'),
            flub:string($DOI),
            $graph)"/>
        <xsl:value-of select="flub:nqliteral($document-uri,
            xs:anyURI('&dct;created'),
            flub:xsd-literal(ARSTALL,'gYear'),
            $graph)"/>
        <xsl:value-of select="flub:nqliteral($document-uri,
            $rdfs-label,
            flub:string(TI_cristin),
            $graph)"/>
        <xsl:value-of select="flub:nqliteral($document-uri,
            xs:anyURI('&upw;vararbnr'),
            flub:string($VARARB),
            $graph)"/>
    
        <!-- begin journal -->        
        <xsl:value-of select="flub:nqobject($journal-uri,
            $rdf-type,
            xs:anyURI('&bibo;Journal'),
            $graph)"/>
        <xsl:value-of select="flub:nqliteral($journal-uri,
            $rdfs-label,
            flub:string(KANALNAVN)
            ,$graph)"/>
        <xsl:value-of select="flub:nqliteral($journal-uri,
            xs:anyURI('&bibo;issn'),
            flub:string($ISSN),
            $graph)"/>
    </xsl:template>
</xsl:stylesheet>